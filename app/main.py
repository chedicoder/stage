from flask_mysqldb import MySQL

from flask import Flask, render_template

app = Flask(__name__, template_folder='templates')

# Configure MySQL
app.config['MYSQL_HOST'] = 'mysql'
app.config['MYSQL_USER'] = 'user'
app.config['MYSQL_PASSWORD'] = 'user'
app.config['MYSQL_DB'] = 'python'

mysql = MySQL(app)

# Routes
@app.route('/')
def index():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM example_table')
    data = cur.fetchall()
    cur.close()
    return render_template('index.html', data=data)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
